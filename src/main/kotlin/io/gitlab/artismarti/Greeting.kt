package io.gitlab.artismarti

/**
 * @author artur
 */
data class Greeting(val content: String = "")
