package io.gitlab.artismarti

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class SpringBootWebsocketMessagingApplication

fun main(args: Array<String>) {
	SpringApplication.run(SpringBootWebsocketMessagingApplication::class.java, *args)
}
