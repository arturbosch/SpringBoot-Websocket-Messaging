package io.gitlab.artismarti

import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.stereotype.Controller

/**
 * @author artur
 */
@Controller
class HelloController {

	@MessageMapping("/hello")
	@SendTo("/topic/greetings")
	fun command(message: HelloMessage): Greeting {
		Thread.sleep(3000)
		return Greeting("Hello, ${message.name}!")
	}

}
