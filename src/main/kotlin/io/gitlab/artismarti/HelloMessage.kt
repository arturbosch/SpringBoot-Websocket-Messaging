package io.gitlab.artismarti

/**
 * @author artur
 */
data class HelloMessage(val name: String = "")
