package io.gitlab.artismarti

import org.apache.log4j.Logger
import org.springframework.messaging.converter.MappingJackson2MessageConverter
import org.springframework.messaging.simp.stomp.StompFrameHandler
import org.springframework.messaging.simp.stomp.StompHeaders
import org.springframework.messaging.simp.stomp.StompSession
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter
import org.springframework.web.socket.client.standard.StandardWebSocketClient
import org.springframework.web.socket.messaging.WebSocketStompClient
import org.springframework.web.socket.sockjs.client.SockJsClient
import org.springframework.web.socket.sockjs.client.Transport
import org.springframework.web.socket.sockjs.client.WebSocketTransport
import java.lang.reflect.Type
import java.util.*

/**
 * @author artur
 */
class StompClientTester {

	companion object {

		@JvmStatic
		fun main(vararg args: String) {
			val transports = ArrayList<Transport>(1)
			transports.add(WebSocketTransport(StandardWebSocketClient()))
			val transport = SockJsClient(transports)

			val stompClient = WebSocketStompClient(transport)
			stompClient.messageConverter = MappingJackson2MessageConverter()

			val url = "ws://localhost:8080/hello"
			val sessionHandler = MyStompSessionHandler()

			val session = stompClient.connect(url, sessionHandler).get() // wait for session

			session.subscribe("/topic/greetings", object : StompFrameHandler {

				override fun getPayloadType(headers: StompHeaders): Type {
					return Greeting::class.java
				}

				override fun handleFrame(headers: StompHeaders, payload: Any) {
					println("Payload: $payload")
				}

			})

			session.send("/app/hello", HelloMessage("Artur"))

			Thread.sleep(2000)

			stompClient.stop { println("Deconnected!") }

		}

	}
}

class MyStompSessionHandler : StompSessionHandlerAdapter() {

	val logger: Logger = Logger.getLogger("Test")

	override fun afterConnected(session: StompSession, connectedHeaders: StompHeaders) {
		logger.debug("Connected!")
	}
}
